RSpec.feature "Products", type: :feature do
  describe 'test' do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before  do
      visit potepan_product_path(product.id)
    end

    scenario '商品詳細ページ表示テスト' do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price
    end

    scenario '一覧ページへ戻る押下テスト' do
      expect(page).to have_link "一覧ページへ戻る"
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
