RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon2) { create(:taxon, name: 'Mugs', parent: taxonomy.root, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "other product", taxons: [other_taxon]) }
  let!(:other_product2) { create(:product, name: 'STEIN', price: 300, taxons: [other_taxon2]) }

  before do
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'カテゴリーページ表示確認テスト' do
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario '関連しない商品の非表示確認テスト' do
    expect(page).to have_no_content other_product.name
  end

  scenario 'サイドバー表示確認テスト' do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
    end
  end

  scenario "サイドバー挙動確認テスト" do
    expect(page).to have_link 'Mugs (1)'
    click_on 'Mugs (1)'
    expect(current_path).to eq potepan_category_path(other_taxon2.id)
    within '.productBox', match: :first do
      expect(page).to     have_content other_product2.name
      expect(page).to     have_content other_product2.price
    end
  end

  scenario '商品詳細ページ遷移確認テスト' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'タイトルがカテゴリ名+サイト名となっていること' do
    expect(page).to have_title("#{taxon.name} - BIGBAG Store")
  end
end
