RSpec.describe ApplicationHelper, type: :helper do
  describe "タイトル表示確認テスト" do
    context "page_titleが空文字の場合" do
      it "titleがBIGBAG Storeとなる" do
        expect(title_name("")).to eq "BIGBAG Store"
      end
    end

    context "page-titleがnilの場合" do
      it "titleがBIGBAG Storeとなる" do
        expect(title_name(nil)).to eq "BIGBAG Store"
      end
    end

    context "page_title-が空でない場合" do
      it "titleがpage_title - BIGBAG Storeとなる" do
        expect(title_name("page_title")).to eq "page_title - BIGBAG Store"
      end
    end
  end
end
