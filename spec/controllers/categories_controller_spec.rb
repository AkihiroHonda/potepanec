RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'showメソッド動作確認テスト' do
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it '正常レスポンス確認' do
      expect(response).to be_successful
    end

    it '@taxonのインスタンス変数確認' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomiesのインスタンス変数確認' do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it '@productsのインスタンス変数確認' do
      expect(assigns(:products)).to contain_exactly product
    end

    it 'show.htmlへのレンダー確認' do
      expect(response).to render_template :show
    end
  end
end
