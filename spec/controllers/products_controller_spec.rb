describe Potepan::ProductsController, type: :controller do
  describe "showメソッド動作確認テスト" do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常レスポンス確認" do
      expect(response).to be_successful
    end

    it '@productのインスタンス変数確認' do
      expect(assigns(:product)).to eq product
    end

    it 'show.htmlへのレンダー確認' do
      expect(response).to render_template :show
    end
  end
end
